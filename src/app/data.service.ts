import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getPosts() {
    return this.http.get('https://jsonplaceholder.typicode.com/posts')
  }

  getPostComent(postId) {
    return this.http.get('https://jsonplaceholder.typicode.com/comments?postId='+postId)
  }

  getPost(postId) {
    return this.http.get('https://jsonplaceholder.typicode.com/posts/'+postId)
  }
}
