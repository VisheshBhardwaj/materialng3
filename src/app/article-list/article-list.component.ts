import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss']
})
export class ArticleListComponent implements OnInit {
  posts$: Object;
  currentUrl: string;
  headerTitle: string;

  constructor(private dataService: DataService,private router: Router,private titleService: Title) { 
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.currentUrl = event.url;
      }
  });
  }

  ngOnInit() {
    if(this.currentUrl==='/'){
      this.headerTitle = "All Posts";
      this.dataService.getPosts().subscribe(
        data => this.posts$ = data 
      );
    }else if(this.currentUrl==='/MyArticles'){
      this.headerTitle = "My Posts";
      this.dataService.getPosts().subscribe(
        data => this.posts$ = data 
      );
    }else if(this.currentUrl==='/MyFavorites'){
      this.headerTitle = "My Favorites Posts";
      this.dataService.getPosts().subscribe(
        data => this.posts$ = data 
      );
    }
    
    this.titleService.setTitle(this.headerTitle);

  }
}
