import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { DataService } from '../data.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  //To-Do Fix the Grid Layout Accross article Component
  post$: Object;
  comments$: Object;
  
  constructor(private route: ActivatedRoute, private data: DataService) { 
     this.route.params.subscribe( params => this.post$=params.id,params => this.comments$ = params.id);
  }

  ngOnInit() {
    this.data.getPost(this.post$).subscribe(
      data => this.post$ = data 
    );
    this.data.getPostComent(this.post$).subscribe(
      data => this.comments$ = data 
    );
  }

}
