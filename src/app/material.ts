import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { NgModule } from '@angular/core';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatDividerModule} from '@angular/material/divider';

@NgModule({
  imports: [MatDividerModule,MatInputModule,MatButtonModule, MatCheckboxModule,MatGridListModule,MatSidenavModule,MatListModule,MatIconModule],
  exports: [MatDividerModule,MatInputModule,MatButtonModule, MatCheckboxModule,MatGridListModule,MatSidenavModule,MatListModule,MatIconModule],
})
export class MaterialModule { }