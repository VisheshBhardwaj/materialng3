import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateArticleComponent } from './create-article/create-article.component'
import { ArticleListComponent } from './article-list/article-list.component'
import { ArticleComponent } from './article/article.component'

const routes: Routes = [
  {path:'articleCreate',component:CreateArticleComponent},
  {path:'',component:ArticleListComponent},
  {path:'MyArticles',component:ArticleListComponent},
  {path:'MyFavorites',component:ArticleListComponent},
  {path:'article/:id',component:ArticleComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
